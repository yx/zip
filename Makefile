CC := tcc

OUT := zip

SRC != find *.c
OBJ := $(SRC:.c=.o)
DEP := $(SRC:.c=.d)

CFLAGS != pkg-config sdl2 --cflags
CFLAGS += -Wall -MD -DSDL_DISABLE_IMMINTRIN_H # weird gccism
LDFLAGS != pkg-config sdl2 --libs
LDFLAGS += -lm

.SUFFIXES: .c .o

$(OUT): $(OBJ)
	$(CC) -o $@ $(OBJ) $(LDFLAGS)

.PHONY: clean run

clean:
	rm -f $(OUT) $(OBJ) $(DEP)

run: $(OUT)
	@./$(OUT)

-include $(DEP)
