#pragma once

#include "camera.h"
#include "level.h"
#include "physics.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// define placement of bits for key char
#define KEY_UP 0x80
#define KEY_LEFT 0x40
#define KEY_RIGHT 0x20

// flags
#define FLAG_CAN_JUMP 0x80

typedef struct {
	Actor orb;
	Actor player;
	Level level;
	Camera camera;
	int currentLevel, maxLevels; // keep track of current level

	char keys; // bit fields
} Game;

void cameraCenterOnActor(Camera *c, Actor *a);
