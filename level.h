#pragma once

#include "physics.h"

typedef struct {
	float spawnX, spawnY;
	float goalX, goalY;
	float width, height; // camera max values
	Actor *platforms; // keep a list of all platforms
	int platformCount;
} Level;

void initLevel(Level *l, char *levelfile); // load platforms from file
void freeLevel(Level *l);

void collideLevelWithActor(Level *l, Actor *a);
