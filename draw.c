#include "draw.h"

void drawActor(SDL_Renderer *ren, Actor *a, Camera *c, unsigned char r, unsigned char g, unsigned char b) {
	SDL_SetRenderDrawColor(ren, r, g, b, 255);
	SDL_Rect rect = { a->x - c->x, a->y - c->y, a->w, a->h };
	SDL_RenderFillRect(ren, &rect);
}
