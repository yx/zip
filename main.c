#include <SDL.h>

#include "draw.h"
#include "game.h"

#define panic(err) { printf("%s:%i - %s", __FILE__, __LINE__, err); exit(1); }

#define FPS 240.0
#define DT (1.0 / FPS)

static char levelfile[8]; // supports up to 99 levels more than enough

int main(int argc, char **argv) {
	if (SDL_Init(SDL_INIT_VIDEO)) panic(SDL_GetError());
	SDL_Window *win = SDL_CreateWindow("Zip", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIN_WIDTH, WIN_HEIGHT, 0);
	if (!win) panic(SDL_GetError());
	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (!ren) panic(SDL_GetError());

	Game game;
	game.level.platforms = NULL; // will ruin arena allocator otherwise
	game.currentLevel = 0;
	game.maxLevels = 2;
	snprintf(levelfile, 8, "level%i", game.currentLevel);
	initLevel(&game.level, levelfile);
	initActor(&game.player, game.level.spawnX, game.level.spawnY, PLAYER_WIDTH, PLAYER_HEIGHT, Player);
	game.player.flags |= FLAG_HOLDING;
	initActor(&game.orb, 0, 0, 10, 10, Orb);
	game.keys = 0;
	game.camera.width = game.level.width;
	game.camera.height = game.level.height;
	cameraCenterOnActor(&game.camera, &game.player);

	float accumulator = 0.0;
	float startTime = SDL_GetPerformanceCounter();

	SDL_Event e;
	while (1) {
		while (SDL_PollEvent(&e)) {
			if (e.type == SDL_QUIT) goto cleanup;
			else if (e.type == SDL_KEYDOWN) {
				switch (e.key.keysym.sym) {
				case SDLK_UP: game.keys |= KEY_UP; break;
				case SDLK_LEFT: game.keys |= KEY_LEFT; break;
				case SDLK_RIGHT: game.keys |= KEY_RIGHT; break;
				default: break;
				}
			} else if (e.type == SDL_KEYUP) {
				switch (e.key.keysym.sym) {
				case SDLK_UP: game.keys &= ~KEY_UP; break;
				case SDLK_LEFT: game.keys &= ~KEY_LEFT; break;
				case SDLK_RIGHT: game.keys &= ~KEY_RIGHT; break;
				default: break;
				}
			} else if (e.type == SDL_MOUSEBUTTONDOWN && e.button.button == SDL_BUTTON_LEFT) { // begin the throw
				if (game.player.flags & FLAG_HOLDING) {
					int mouseX = e.motion.x + game.camera.x, mouseY = e.motion.y + game.camera.y;
					float dx = mouseX - game.orb.x;
					float dy = mouseY - game.orb.y;
					float mag = sqrtf(dx * dx + dy * dy); // normalize and scale
					if (mag * ORB_THROW_SCALE < ORB_MAX_THROW_VELOCITY) {
						game.orb.dx = dx * ORB_THROW_SCALE;
						game.orb.dy = dy * ORB_THROW_SCALE;
					} else {
						game.orb.dx = dx / mag * ORB_MAX_THROW_VELOCITY;
						game.orb.dy = dy / mag * ORB_MAX_THROW_VELOCITY;
					}
					game.player.flags &= ~FLAG_HOLDING;
				}
			}
		}
		const float currentTime = SDL_GetPerformanceCounter();
		accumulator += (currentTime - startTime) / (float)SDL_GetPerformanceFrequency();
		startTime = currentTime;
		if (accumulator > FPS * 2) accumulator = FPS * 2; // clamp accumulator in case of spiral of death

		while (accumulator > DT) {
			game.player.dy += GRAVITY * DT;
			if ((game.keys & KEY_UP) && (game.player.flags & FLAG_GROUNDED)) game.player.dy = -PLAYER_JUMP_SPEED;
			if (game.player.flags & FLAG_GROUNDED) game.player.dx += (((game.keys & KEY_RIGHT) > 0) - ((game.keys & KEY_LEFT) > 0)) * PLAYER_MOVE_SPEED * DT;
			moveActor(&game.player, DT);
			if (game.player.flags & FLAG_HOLDING) {
				game.orb.x = game.player.x + game.player.w / 2 - game.orb.w / 2; 
				game.orb.y = game.player.y + game.player.h / 2;
				cameraCenterOnActor(&game.camera, &game.player); // follow player
			} else {
				if (fabs(game.orb.dx) < 1 && fabs(game.orb.dy) < 1) { // if the orb stops moving, teleport
					game.player.x = game.orb.x + game.orb.w - game.player.w;
					game.player.y = game.orb.y + game.orb.h - game.player.h;
					game.player.flags |= FLAG_HOLDING;
				}
				game.orb.dy += GRAVITY * DT;
				moveActor(&game.orb, DT);
				cameraCenterOnActor(&game.camera, &game.orb); // follow orb, we're throwing
			}
			collideLevelWithActor(&game.level, &game.player);
			collideLevelWithActor(&game.level, &game.orb);

			if (game.player.x + game.player.w > game.level.goalX && game.level.goalX + GOAL_WIDTH > game.player.x && game.player.y + game.player.h > game.level.goalY && game.level.goalY + GOAL_HEIGHT > game.player.y) {
				game.currentLevel++;
				if (game.currentLevel < game.maxLevels) {
					snprintf(levelfile, 8, "level%i", game.currentLevel);
					initLevel(&game.level, levelfile);
					initActor(&game.player, game.level.spawnX, game.level.spawnY, PLAYER_WIDTH, PLAYER_HEIGHT, Player);
					game.player.flags |= FLAG_HOLDING; // give us the orb back
					game.camera.width = game.level.width;
					game.camera.height = game.level.height;
					cameraCenterOnActor(&game.camera, &game.player);
					continue; // just in case, I got a weird segfault that may be caused by this
				}
			} // handle goals

			if (game.player.x < 0 || game.player.x > game.camera.width || game.player.y < 0 || game.player.y > game.camera.height || game.orb.x < 0 || game.orb.x > game.camera.width || game.orb.y < 0 || game.orb.y > game.camera.height) {
				initActor(&game.player, game.level.spawnX, game.level.spawnY, PLAYER_WIDTH, PLAYER_HEIGHT, Player);
				game.player.flags |= FLAG_HOLDING;
			}
			accumulator -= DT;
		}

		SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
		SDL_RenderClear(ren);

		SDL_Rect r = { game.level.goalX - game.camera.x, game.level.goalY - game.camera.y, GOAL_WIDTH, GOAL_HEIGHT };
		SDL_SetRenderDrawColor(ren, 0, 180, 0, 255);
		SDL_RenderFillRect(ren, &r);
		drawActor(ren, &game.player, &game.camera, 120, 0, 140);
		drawActor(ren, &game.orb, &game.camera, 0, 0, 120);
		for (int i = 0; i < game.level.platformCount; i++) {
			drawActor(ren, &game.level.platforms[i], &game.camera, 120, 0, 0);
		}
		
		SDL_RenderPresent(ren);
	}

cleanup:
	SDL_DestroyWindow(win);
	SDL_DestroyRenderer(ren);
	SDL_Quit();
	freeLevel(&game.level);
	return 0;
}
