#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "level.h"

void initLevel(Level *l, char *levelfile) {
	FILE *f = fopen(levelfile, "r");

	// get line count
	int linecount = 0;
	char ch;
	while ((ch = fgetc(f)) != EOF) if (ch == '\n') linecount++;
	l->platformCount = linecount - 2; // first line is spawn coord, second is goal pos
	free(l->platforms);
	l->platforms = malloc(sizeof(Actor) * l->platformCount);
	
	rewind(f); 
	char *line = NULL;
	size_t linesize = 0;
	ssize_t linelen;
	linelen = getline(&line, &linesize, f);
	float sx = atof(strtok(line, " "));
	float sy = atof(strtok(NULL, " "));
	l->spawnX = sx;
	l->spawnY = sy;

	linelen = getline(&line, &linesize, f);
	l->goalX = atof(strtok(line, " "));
	l->goalY = atof(strtok(NULL, " "));

	float maxX = 0;
	float maxY = 0;
	int i = 0;
	while ((linelen = getline(&line, &linesize, f)) != -1) {
		float x = atof(strtok(line, " "));
		float y = atof(strtok(NULL, " "));
		float w = atof(strtok(NULL, " "));
		float h = atof(strtok(NULL, " "));
		if (x + w > maxX) maxX = x + w;
		if (y + h > maxY) maxY = y + h;
		int mat = atoi(strtok(NULL, " "));
		initActor(&l->platforms[i], x, y, w, h, mat);
		i++;
	}
	l->width = maxX;
	l->height = maxY;
	free(line);
	
	fclose(f);
}

void freeLevel(Level *l) {
	free(l->platforms);
}

void collideLevelWithActor(Level *l, Actor *a) {
	for (int i = 0; i < l->platformCount; i++) {
		collideActors(a, &l->platforms[i]);
	}
}
