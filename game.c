#include <math.h>

#include "game.h"

void cameraCenterOnActor(Camera *c, Actor *a) {
	c->x = fmax(0, fmin(a->x + a->w - WIN_WIDTH / 2, c->width - WIN_WIDTH));
	c->y = fmax(0, fmin(a->y + a->h - WIN_HEIGHT / 2, c->height - WIN_HEIGHT));
}
