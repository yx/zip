# Zip - A puzzle platformer
# Very WIP

# Concept
Zip is based off of a relatively simple but interesting mechanic: the player has a projectile that, when thrown, will teleport the player to its final resting position. This results in an interesting system with a lot of room for interesting and challenging puzzles. Right now the core gameplay is being implemented, which will likely undergo a lot of changes as I work out the kinks. It's nowhere near ready to play, but might still be interesting nonetheless.

# Building
Zip builds on any system with a C99 compiler and make, as well as SDL2 support. I reccomend TCC, as that is what I personally used for this project, but other C compilers should work as well, just change `CC` in the makefile. To build the game just run `make`, the binary generated will be `./zip`. You can also run the project by running `make run` which will build and run the game automatically.
