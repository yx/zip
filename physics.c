#include <math.h>

#include "physics.h"

const float restitutions[] = { // 2.0 is a placeholder to ensure that springs work
	2.0, // player
	2.0, // orb
	0.2, // standard floor
};

const float densities[] = {
	1.4, // player
	0.4, // orb
	0.0, // standard floor
};

const float frictions[] = {
	0.0, // player
	0.001, // orb
	0.03, // standard floor
};

void initActor(Actor *a, float x, float y, float w, float h, Material m) {
	a->x = x;
	a->y = y;
	a->w = w;
	a->h = h;

	a->dx = 0;
	a->dy = 0;

	a->mass = w * h * densities[m];
	a->restitution = restitutions[m];
	a->friction = frictions[m];
	a->flags = 0;
}

void moveActor(Actor *a, float dt) {
	a->x += a->dx * dt;
	a->y += a->dy * dt;
	a->flags &= ~FLAG_GROUNDED; // always reset grounded, we don't want to jump when we shouldn't
}

void collideActors(Actor *a, Actor *b) {
	if (a->x + a->w > b->x && b->x + b->w > a->x && a->y + a->h > b->y && b->y + b->h > a->y) { // we got a collision
		float dx, dy; // get overlaps
		if (a->x < b->x) dx = a->x + a->w - b->x;
		else dx = b->x + b->w - a->x;
		if (a->y < b->y) dy = a->y + a->h - b->y;
		else dy = b->y + b->h - a->y;
		float res = fmin(a->restitution, b->restitution); // use lowest restitution
		float friction = 1.0 - fmax(a->friction, b->friction); // use highest friction value
		
		if (dx < dy) { // resolve x collisions
			float momentum = a->mass * a->dx + b->mass * b->dx;
			momentum *= res;
			if (a->mass) a->dx = -momentum / a->mass;
			if (b->mass) b->dx = momentum / b->mass;
			if (a->x < b->x) a->x = b->x - a->w;
			else a->x = b->x + b->w;
			
			a->dy *= friction;
			b->dy *= friction;
		} else {
			if (b->mass == 0.0 && a->dy > 0) { // we landed on some sort of platform
				a->flags |= FLAG_GROUNDED;
			}
			float momentum = a->mass * a->dy + b->mass * b->dy;
			momentum *= res;
			if (a->mass) a->dy = -momentum / a->mass;
			if (b->mass) b->dy = momentum / b->mass;
			if (a->y < b->y) a->y = b->y - a->h;
			else a->y = b->y + b->h;

			a->dx *= friction;
			b->dx *= friction;

		}
	}
}
