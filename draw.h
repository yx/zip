#pragma once

#include <SDL.h>

#include "camera.h"
#include "physics.h"

void drawActor(SDL_Renderer *ren, Actor *a, Camera *c, unsigned char r, unsigned char g, unsigned char b);
