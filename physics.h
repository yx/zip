#pragma once

#define FLAG_GROUNDED 0x80

// player specific flag
#define FLAG_HOLDING 0x40

#define PLAYER_WIDTH 20
#define PLAYER_HEIGHT 30

#define GOAL_WIDTH 30
#define GOAL_HEIGHT 40

#define GRAVITY 750
#define PLAYER_MOVE_SPEED 2000
#define PLAYER_JUMP_SPEED 400
#define ORB_MAX_THROW_VELOCITY 600
#define ORB_THROW_SCALE 2

typedef enum {
	Player,
	Orb,
	StandardFloor,
} Material;

typedef struct {
	float x, y;
	float w, h; // bounding rect
	float dx, dy;

	float mass; // impulse and momentum stuff
	float restitution;
	float friction; // since forces are sorta annoying this is fake friction
	char flags; // store other object information
} Actor; // used for active objects (tiles will be handled differently)

void initActor(Actor *a, float x, float y, float w, float h, Material m);
void moveActor(Actor *a, float dt);
void collideActors(Actor *a, Actor *b);
